package de.samply.cadsr;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;

import de.samply.cadsr.CadsrElement.DescribedValueDomain;
import de.samply.cadsr.CadsrElement.PermissibleValueItem;
import de.samply.cadsr.CadsrElement.ValueDomainType;

/**
 * A simple REST client to the caDSR metadata repository.
 */
public class CadsrClient {

    private static final Logger logger = LogManager.getLogger(CadsrClient.class);

    /**
     * The HTTP client for REST
     */
    private Client client;

    /**
     * The base CDE url (to get data elements)
     */
    private static final String baseCDEUrl = "https://cadsrapi.nci.nih.gov/cderest/rest/services/";

    /**
     * The base DSR url (to get described value domains)
     */
    private static final String baseDSRUrl = "http://cadsrapi.nci.nih.gov/cadsrapi41/GetXML";

    /**
     * The element cache, key: public Id, value: CadsrElement
     */
    private static Cache<Object, Object> elementCache = CacheBuilder.newBuilder().maximumSize(1000).build();

    /**
     * The node cache used for cache parsed elements. This cache is also used by the elementCache.
     */
    private static Cache<Object, Object> nodeCache = CacheBuilder.newBuilder().maximumSize(1000).build();

    /**
     * The search cache used for search requests.
     */
    private static Cache<Object, Object> searchCache = CacheBuilder.newBuilder().maximumSize(1000).build();

    /**
     * The value domain cache used for described value domains.
     */
    private static Cache<Object, Object> valueDomainCache = CacheBuilder.newBuilder().maximumSize(1000).build();

    /**
     * Initializes a new CadsrClient
     * @param client
     */
    public CadsrClient(Client client) {
        this.client = client;
    }

    /**
     * Returns the caDSR element with the given public ID
     * @param publicId
     * @return
     * @throws Exception
     */
    public CadsrElement getElement(final String publicId) throws Exception {
        return (CadsrElement) elementCache.get(publicId, new Callable<Object>() {
            @Override
            public Object call() throws Exception {
                if(nodeCache.getIfPresent(publicId) != null) {
                    return nodeCache.getIfPresent(publicId);
                }
                return getDataElement(publicId);
            }
        });
    }

    /**
     * Loads the next elements for the given search.
     * @param search
     * @return
     * @throws Exception
     */
    public CadsrSearch loadNext(CadsrSearch search) throws Exception {
        /**
         * Make sure only one thread tries to load the next block...
         */
        synchronized (search) {
            if(search.getNext() == null || search.isDone()) {
                search.setDone(true);
                return search;
            }

            String xml = getXML(client.target(search.getNext()));
            XPath xPath = XPathFactory.newInstance().newXPath();

            Document document = loadXMLFromString(xml);

            Node linkNext = (Node) xPath.compile("//queryResponse/next").evaluate(document, XPathConstants.NODE);
            Node response = (Node) xPath.compile("//queryResponse").evaluate(document, XPathConstants.NODE);

            if(response == null) {
                search.setDone(true);
                return search;
            }

            search.setTotal(Integer.parseInt(xPath.compile("recordCounter/text()").evaluate(response)));

            if(linkNext != null) {
                search.setNext(linkNext.getAttributes().getNamedItem("xlink:href").getTextContent());
            } else {
                search.setNext(null);
            }

            List<CadsrElementDescription> target = new ArrayList<>();

            NodeList list = (NodeList) xPath.compile("//queryResponse/class[@name='gov.nih.nci.cadsr.domain.DataElement']").evaluate(document, XPathConstants.NODESET);

            XPathExpression idXPath = xPath.compile("field[@name='publicID']/text()");
            XPathExpression longNameXPath = xPath.compile("field[@name='longName']/text()");
            XPathExpression definitionXPath = xPath.compile("field[@name='preferredDefinition']/text()");

            for(int i = 0; i < list.getLength(); ++i) {
                Node item = list.item(i);
                CadsrElementDescription desc = new CadsrElementDescription();
                desc.setPublicId(idXPath.evaluate(item));
                desc.setDefinition(definitionXPath.evaluate(item));
                desc.setDesignation(longNameXPath.evaluate(item));
                target.add(desc);
            }

            search.getElements().addAll(target);

            search.setDone(search.getTotal() == search.getElements().size());

            return search;
        }
    }

    /**
     * Searches the cadsr for the given input string. (Cached)
     * @param input
     * @return
     * @throws Exception
     */
    public CadsrSearch search(final String input) throws Exception {
        return (CadsrSearch) searchCache.get(input, new Callable<Object>() {
            @Override
            public Object call() throws Exception {
                CadsrSearch result = new CadsrSearch();
                result.setNext(getDSRTarget().queryParam("query",
                        String.format("DataElement[@longName=%s][@workflowStatusName=RELEASED]", input)).getUri().toString());
                return loadNext(result);
            }
        });
    }

    /**
     * Searches the cadsr for the data element with the given public ID. (Cached)
     * @param publicId
     * @return
     * @throws Exception
     */
    private CadsrElement getDataElement(String publicId) throws Exception {
        logger.debug("Getting element with public ID " + publicId);

        String xml = getXML(getDataElementTarget().queryParam("publicId",
                publicId));

        Document document = loadXMLFromString(xml);

        NodeList list = document.getElementsByTagName("DataElement");

        if(list.getLength() != 1) {
            logger.error("Element with public ID " + publicId + " not found!");
            return null;
        }

        return parseElement(list.item(0), false);
    }

    /**
     * Parses the given Node into a CadsrElement. (Cached)
     * @param item
     * @param lazy
     * @return
     * @throws Exception
     */
    private CadsrElement parseElement(final Node item, boolean lazy) throws Exception {
        final XPath xPath = XPathFactory.newInstance().newXPath();

        final String publidId = xPath.compile("PUBLICID/text()").evaluate(item);
        final String version = xPath.compile("VERSION/text()").evaluate(item);

        return (CadsrElement) nodeCache.get(publidId, new Callable<Object>() {
            @Override
            public Object call() throws Exception {
                CadsrElement element = new CadsrElement();

                element.setPublicId(publidId);

                String datatype = xPath.compile("VALUEDOMAIN/Datatype/text()")
                        .evaluate(item);
                String designation = xPath.compile("LONGNAME/text()").evaluate(item);
                String definition = xPath.compile("PREFERREDDEFINITION/text()")
                        .evaluate(item);

                element.setDatatype(datatype);
                element.setDesignation(designation);
                element.setDefinition(definition);
                element.setVersion(version);

                Node valuesList = (Node) xPath.compile("VALUEDOMAIN/PermissibleValues")
                        .evaluate(item, XPathConstants.NODE);

                final String valueDomainId = xPath.compile("VALUEDOMAIN/PublicId/text()").evaluate(item);
                element.setValueDomainPublicId(valueDomainId);

                if (valuesList.hasChildNodes()) {
                    element.setType(ValueDomainType.ENUMERATED);
                    logger.debug("Element has an enumerated value domain, parsing the list of permissible values");

                    NodeList listValues = (NodeList) xPath.compile("PermissibleValues_ITEM").evaluate(valuesList, XPathConstants.NODESET);

                    for(int i = 0; i < listValues.getLength(); ++i) {
                        Node node = listValues.item(i);
                        PermissibleValueItem validItem = new PermissibleValueItem();
                        validItem.setValue(xPath.compile("VALIDVALUE/text()").evaluate(node));
                        validItem.setDefinition(xPath.compile("MEANINGDESCRIPTION/text()").evaluate(node));
                        validItem.setDesignation(xPath.compile("VALUEMEANING/text()").evaluate(node));
                        element.getPermissibleValues().add(validItem);
                    }
                } else {
                    element.setType(ValueDomainType.DESCRIBED);
                    logger.debug("Element has a described value domain with a public ID " + valueDomainId);

                    DescribedValueDomain valueDomain = (DescribedValueDomain) valueDomainCache.get(valueDomainId, new Callable<Object>() {
                        @Override
                        public Object call() throws Exception {
                            String xml = getXML(getDSRTarget().queryParam("query", "ValueDomain[@publicId=" + valueDomainId + "]"));
                            DescribedValueDomain domain = new DescribedValueDomain();

                            Document document = loadXMLFromString(xml);

                            domain.setFormat(xPath.compile("//queryResponse/class/field[@name='formatName']/text()")
                                    .evaluate(document));
                            domain.setMaxCharacters(xPath.compile("//queryResponse/class/field[@name='maximumLengthNumber']/text()")
                                    .evaluate(document));
                            return domain;
                        }
                    });

                    element.setFormat(valueDomain.getFormat());
                    element.setMaxCharacters(valueDomain.getMaxCharacters());
                }

                return element;
            }
        });
    }

    /**
     * Returns a WebTarget for the CDE REST interface
     * @return
     */
    private WebTarget getDataElementTarget() {
        return client.target(baseCDEUrl).path("getDataElement")
                .queryParam("format", "xml");
    }

    /**
     * Returns a WebTarget for the DSR REST interface
     * @return
     */
    private WebTarget getDSRTarget() {
        return client.target(baseDSRUrl);
    }

    /**
     * Returns the XML response of the given WebTarget.
     * @param target
     * @return
     * @throws ExecutionException
     */
    private String getXML(final WebTarget target) throws ExecutionException {
        logger.debug("Getting URL " + target.getUri().toString());

        Response response = target.request(MediaType.APPLICATION_XML).get();

        if (response.getStatus() != 200) {
            throw new WebApplicationException(response.getStatus());
        }

        return response.readEntity(String.class);
    }

    /**
     * Loads the given string into an XML document and returns it.
     * @param xml
     * @return
     * @throws Exception
     */
    private Document loadXMLFromString(String xml) throws Exception {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setNamespaceAware(true);
        DocumentBuilder builder = factory.newDocumentBuilder();
        InputSource is = new InputSource(new StringReader(xml));
        return builder.parse(is);
    }

}

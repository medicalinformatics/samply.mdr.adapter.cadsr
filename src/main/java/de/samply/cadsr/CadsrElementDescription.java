package de.samply.cadsr;

import java.io.Serializable;

public class CadsrElementDescription implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * Its public ID
     */
    private String publicId;

    /**
     * Its definition (PREFERREDDEFINITION)
     */
    private String definition;

    /**
     * Its designation (LONGNAME)
     */
    private String designation;

    /**
     * @return the definition
     */
    public String getDefinition() {
        return definition;
    }

    /**
     * @param definition the definition to set
     */
    public void setDefinition(String definition) {
        this.definition = definition;
    }

    /**
     * @return the designation
     */
    public String getDesignation() {
        return designation;
    }

    /**
     * @param designation the designation to set
     */
    public void setDesignation(String designation) {
        this.designation = designation;
    }

    /**
     * @return the publicId
     */
    public String getPublicId() {
        return publicId;
    }

    /**
     * @param publicId the publicId to set
     */
    public void setPublicId(String publicId) {
        this.publicId = publicId;
    }

}

package de.samply.cadsr;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * A search request for the caDSR.
 */
public class CadsrSearch implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * If true, the search request has been loaded completely.
     */
    private boolean done = false;

    /**
     * The total number of data elements.
     */
    private int total;

    /**
     * The URL to get the next block of data elements. Null if there is none.
     */
    private String next;

    /**
     * The (incomplete) list of elements returned by this search request.
     */
    private List<CadsrElementDescription> elements = new ArrayList<>();

    /**
     * @return the done
     */
    public boolean isDone() {
        return done;
    }

    /**
     * @param done the done to set
     */
    public void setDone(boolean done) {
        this.done = done;
    }

    /**
     * @return the total
     */
    public int getTotal() {
        return total;
    }

    /**
     * @param total the total to set
     */
    public void setTotal(int total) {
        this.total = total;
    }

    /**
     * @return the next
     */
    public String getNext() {
        return next;
    }

    /**
     * @param next the next to set
     */
    public void setNext(String next) {
        this.next = next;
    }

    /**
     * @return the elements
     */
    public List<CadsrElementDescription> getElements() {
        return elements;
    }

    /**
     * @param elements the elements to set
     */
    public void setElements(List<CadsrElementDescription> elements) {
        this.elements = elements;
    }

}
